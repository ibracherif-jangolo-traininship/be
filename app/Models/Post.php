<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     schema="Post",
 *     description="Posts",
 *     @OA\Property(type="integer", property="title"),
 *     @OA\Property(type="string", property="description"),
 * )
 * Class Post
 * @package App\Models
 */
class Post extends Model
{
    use HasFactory;
    protected $fillable = [
        'title', 'description'
    ];
}